from flask import Flask, request, render_template
import logging
import sqlite3
import csv
from multiprocessing.pool import ThreadPool as Pool
# from multiprocessing import Pool
from random import randint
from time import sleep


DB_NAME = "employee.db"
app = Flask(__name__, template_folder='frontend')


logging.basicConfig(filename='demo.log',
level=logging.DEBUG,
format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

@app.route("/")
def index(): 
    """
    Showing the home page with necessary links.
    """
    app.logger.info('Processing default request to home page')           
    return render_template('index.html')

def find_manager(man):
    """
    Function to append list of manager to db.
    input : manager name
    output : List of manager above it.
    """
    lst = ""
    records = ""
    conn = sqlite3.connect("employee.db")
    # conn.row_factory = sqlite3.Row
    lst = lst + man + " "
    cursor = conn.cursor()
    conn.row_factory = sqlite3.Row  
    cursor.execute("SELECT manager from Employees where name LIKE '%"+man+"%' ")
    records = cursor.fetchall()
    if len(records) == 0:
        return lst
    print("query run of finding list of managers completed")
    print(records[0][0])
    lst = lst + records[0][0]
    app.logger.info("List of managers above the employee {}".format(lst))           
    return lst

@app.route("/add")
def add():
    """
    Function to get parameters to insert details of employee.
    """
    return render_template('add.html')

def get_next_line(file1):
    """
    File is given as input and data is sent line by line.
    """
    with open(file1, 'r') as f:
        dr = csv.DictReader(f) # comma is default delimiter
        to_db = [(i['empname'],i['dept'],i['role'], i['manager']) for i in dr]
        print("inside todb")
        print(to_db)
        return to_db


def process_line(l):
    """
    Function to add the obtained line in db with validations of whether
    employee already existed or manager is present in db.
    """    
    with sqlite3.connect(DB_NAME) as con:
        cur = con.cursor()
        print("before query")
        cur.execute("SELECT EXISTS(SELECT manager FROM Employees WHERE name LIKE '%"+l[3]+"%') ")
        xyz = cur.fetchall()
        print("result is ",xyz[0][0])
        cur.execute("SELECT EXISTS(SELECT name FROM Employees WHERE name LIKE '%"+l[0]+"%') ")
        abc = cur.fetchall()
        print("result is ",abc[0][0])
        if xyz[0][0] == 1 and abc[0][0] == 0: 
            lst1 = find_manager(l[3])
            print(lst1)  
            cur.execute("INSERT INTO EMPLOYEES(name, department, role, manager) values(?,?,?,?)", (l[0], l[1], l[2], lst1))
            con.commit()
            msg = "Employees Successfully Added!"
        else:
            con.rollback()  
            msg = "We can not add the employee to the list as its manager doesnt exist or employee already existed"
    return msg


@app.route("/savecsv", methods=["POST", "GET"])
def savecsv():
    """
    Function to obtain csv file as input and process each line through
    multithreading.
    """
    file1 = request.form["file"] 
    f = get_next_line(file1) 
    t = Pool(processes=4)
    for i in f:
        msg = t.map(process_line, (i,))
    t.close()
    t.join()
    app.logger.info("csv file {} uploaded".format(file1))           
    return render_template('success.html',msg = msg)

@app.route("/details", methods = ["POST","GET"])  
def details():
    """
    Function to show hierarchy of the employee searched.
    """
    x = ""    
    search = request.form["search"]  
    con = sqlite3.connect("employee.db")
    con.row_factory = sqlite3.Row  
    cur = con.cursor()  
    cur.execute("select manager from Employees where name LIKE '%"+search+"%' ")  
    rows = cur.fetchall()
    if len(rows) == 0:
        x = "Your search was not found!!!Try again!!!"
        return render_template("details.html",msg = x)
    print(rows[0][0])
    x = x + rows[0][0]
    x = x.split(' ')
    x.reverse()
    x.append(search)
    app.logger.info("Employee {} has hierarchy {}".format(search,x))           
    return render_template("details.html",rows = x)
    con.close()




@app.route("/savedetails", methods=["POST", "GET"])
def savedetails():
    """
    Function to save obtained details from the form in db.
    """
    msg = "msg"

    if request.method == "POST":
        try:
            lst1 = ""
            name = request.form["name"]
            department = request.form["department"]
            role = request.form["role"]
            manager = request.form["manager"]
            with sqlite3.connect(DB_NAME) as con:
                cur = con.cursor()
                print("before query")
                cur.execute("SELECT EXISTS(SELECT manager FROM Employees WHERE name LIKE '%"+manager+"%') ")
                xyz = cur.fetchall()
                print("result is ",xyz[0][0])
                cur.execute("SELECT EXISTS(SELECT name FROM Employees WHERE name LIKE '%"+name+"%') ")
                abc = cur.fetchall()
                print("result is ",abc[0][0])

                if xyz[0][0] == 1 and abc[0][0] == 0: 
                    lst1 = find_manager(manager)
                    print(lst1)  
                    cur.execute("INSERT INTO EMPLOYEES(name, department, role, manager) values(?,?,?,?)", (name, department, role, lst1))
                    con.commit()
                    msg = "Employee Successfully Added!"
                    app.logger.info("Employee details added succesfully.")           
                else:
                    con.rollback()  
                    msg = "We can not add the employee to the list as its manager doesnt exist or employee already exist"
                    app.logger.info("Employee details were not added succesfully.")           

        except Exception:
            con.rollback()
            app.logger.info("Employee details had a problem!!!so exception occurred")           
            msg = "We can not add the employee to the list"

        finally:
            return render_template("success.html", msg=msg)
            con.close()


@app.route("/view")
def view():
    """
    Function to view full list of employees.
    """
    con = sqlite3.connect(DB_NAME)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute("select * from Employees")
    rows = cur.fetchall()
    app.logger.info("Full list of employee shown")           
    return render_template("view.html", rows=rows)


@app.route("/delete")
def delete():
    """
    An employee to be deleted based on name obtained in text.
    """
    app.logger.info("Getting employee info to delete the record!!")           
    return render_template('delete.html')


@app.route("/deleterecord", methods=["POST"])
def deleterecord():
    """
    Deleting employee info to remove from manager list and rearrange it
    and delete the reccorde of it.
    """
    try:
        with sqlite3.connect(DB_NAME) as con:
            name = request.form["name"]
            cur = con.cursor()
            print("query")
            cur.execute("SELECT EXISTS(SELECT name FROM Employees WHERE name LIKE '%"+name+"%') ")
            xyz = cur.fetchall()
            print("result is ",xyz[0][0])
            cur.execute("SELECT EXISTS(SELECT manager FROM Employees WHERE manager LIKE '%"+name+"%') ")
            abc = cur.fetchall()
            print("result is ",abc[0][0])
            if abc[0][0] == 1:
                manager =  ""
                cur.execute("select manager from Employees where name LIKE '%"+name+"%' ")  
                sam = cur.fetchall()
                manager = manager + sam[0][0]
                lst1 = ""
                print(manager)
                lst1 = find_manager(manager)
                print(lst1)
                cur.execute("UPDATE Employees SET manager = '"+lst1+"'  where manager LIKE '%"+name+"%'  ")
                app.logger.info("rearranging employee details due to deletion of the record!!")           

                print("deleted")
                msg = "record successfully deleted"

            if xyz[0][0] == 1:
                cur.execute("delete from Employees where name LIKE '%"+name+"%' ")
                print("deleted")
                app.logger.info("employee info successfully deleted!!")           
                msg = "record successfully deleted"
            
            if xyz[0][0] == 0 and abc[0][0] == 0:
                msg = "employee details not found"
                
    except Exception:
        msg = "can't be deleted"
    finally:
        return render_template("delete_record.html", msg=msg)


if __name__ == "__main__":
    app.run(debug=True)
